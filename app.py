from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return jsonify({
        'message': 'Hello Reema'
        }), 200


# Default port:
if __name__ == '__main__':
    app.debug = True
    app.run()
