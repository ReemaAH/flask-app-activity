import json
import unittest

from app import app


class FlaskAppTestCase(unittest.TestCase):
    """This class represents the CastingAgency test cases"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = app
        self.client = self.app.test_client

    def test_main_endpoint(self):
        """ Test get actors failure """
        # get response data when there are no actors in DB
        response = self.client().get('/')
        self.assertEqual(json.loads(response.data)['message'], 'Hello Reema')
        self.assertEqual(response.status_code, 200)


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
